<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>En este ejercicio vamos a escribir un texto en la página web a traves de PHP</div>
        <hr>
        
        <?php
            echo "Este texto está escrito desde el script de PHP.";
            
            echo "Este texto también está escrito desde el script de PHP.";
        
        ?>
        
        <hr>
        Esto está escrito en HTML normal
        <hr>
        <?php
        
            ######################################################
            ######### Este comentario  es de una sola línea#######
            ######################################################
        
        # En una página podemos colocar tantos scripts de PHP como se desee
        
        print("Esta es otra forma de escribir  osas en la web");
        ?>
    </body>
</html>
